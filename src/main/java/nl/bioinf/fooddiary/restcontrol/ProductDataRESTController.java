package nl.bioinf.fooddiary.restcontrol;

import nl.bioinf.fooddiary.FooddiaryApplication;
import nl.bioinf.fooddiary.dao.ProductRepository;
import nl.bioinf.fooddiary.model.product.ProductDescription;
import nl.bioinf.fooddiary.model.product.ProductDescriptionWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

/**
 * @author Tom Wagenaar
 * Date (latest update): 17-03-2022
 *
 * This class is a REST API (REpresentional State Transfer), it receives requests from the front-end through different
 * HTTP requests using the URL. These requests are then used to retrieve the corresponding database data. This class
 * listens to the /api/v1/productDescriptions endpoint and returns product description data, the total number of
 * products in the database and the total number of products displayed.
 */
@RestController
@RequestMapping(value = "/api/v1")
public class ProductDataRESTController {

    private static final Logger logger = LoggerFactory.getLogger(FooddiaryApplication.class);

    @Autowired
    private ProductRepository productRepository;

    /**
     * This REST API listens to the endpoint /api/v1/productDescriptions with optional path variables
     * ?page=<number>&size<number> that can be specified in the URL. This method calls the ProductRepository interface
     * to retrieve the total number of products from the database and the products that fall between the page and size
     * range specified by the path variables. Additionally, it creates a wrapper object and adds links to it based on
     * calculations.
     *
     * @param page (int) -> page number specified in the endpoint, default value is 0.
     * @param size (int) -> number of products displayed per page specified in the endpoint, default value is 10.
     * @return ResponseEntity<ProductDescriptionWrapper> -> entity that contains the requested data and an OK status code.
     */
    @RequestMapping(value = "/productDescriptions", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<ProductDescriptionWrapper> productDescriptions(@RequestParam(defaultValue = "0") int page,
                                                                        @RequestParam(defaultValue = "10") int size) {
        logger.info("/api/v1/productDescriptions endpoint called!");

        // Retrieve the total number of products in the database.
        int totalNumberProducts = productRepository.getTotalNumberProducts();

        logger.info("Retrieved total number of products from the database.");

        // Check if the values that are specified in the path variables are valid, otherwise replace both the page and
        // the size variable or only the size variable.
        if (page > 0 && size > totalNumberProducts) {
            logger.info("Path variable page and size aren't valid, overwriting page variable with value 0 and size variable with value " + totalNumberProducts);
            page = 0;
            size = totalNumberProducts;
        } else if (size > totalNumberProducts) {
            size = totalNumberProducts;
        }

        logger.info("Checked if path variables are valid.");

        // Retrieve products from the database.
        List<ProductDescription> productDescriptionList = productRepository.getAllProductDescriptions(page, size);

        logger.info("Retrieved products from the database.");

        // Create a wrapper object that contains the products, the total number of products and the number of products
        // that are displayed.
        ProductDescriptionWrapper wrapper = ProductDescriptionWrapper.builder(productDescriptionList, totalNumberProducts, size).build();

        // Add a self link containing the API endpoint to the wrapper object.
        wrapper.add(linkTo(methodOn(ProductDataRESTController.class).productDescriptions(page, size)).withSelfRel());

        // Do a calculation that is used to determine if a link to the next page and or a link to th previous page should should be returned to the front-end.
        // be added to the wrapper object. Note, page+1 is used because the first page is page 0.
        if (page != 0 && ((page+1) * size) < totalNumberProducts) {
            logger.info("Add a next and a previous page to the wrapper object.");
            wrapper.add(linkTo(methodOn(ProductDataRESTController.class).productDescriptions(page + 1, size)).withRel("next"));
            wrapper.add(linkTo(methodOn(ProductDataRESTController.class).productDescriptions(page - 1, size)).withRel("previous"));
        } else if (page == 0 && ((page+1) * size) < totalNumberProducts) {
            logger.info("Add a next to the wrapper object.");
            wrapper.add(linkTo(methodOn(ProductDataRESTController.class).productDescriptions(page + 1, size)).withRel("next"));
        } else if (page != 0 && ((page+1) * size) > totalNumberProducts) {
            logger.info("Add a previous page to the wrapper object.");
            wrapper.add(linkTo(methodOn(ProductDataRESTController.class).productDescriptions(page - 1, size)).withRel("previous"));
        }

        return new ResponseEntity<>(wrapper, HttpStatus.OK);
    }
}
