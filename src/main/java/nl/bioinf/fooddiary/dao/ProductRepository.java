package nl.bioinf.fooddiary.dao;

import nl.bioinf.fooddiary.model.product.*;

import java.sql.Date;
import java.util.List;

/**
 * @author Tom Wagenaar
 *
 * Interface that support the ProductDAO, this interface is used in the data controller to call the methods in the
 * ProductDAO. This interface supports inserting the product data into the database when called.
 */
public interface ProductRepository {

    // WebBasedInfSys2 method's that are used:
    Product getProductByDescriptionDutch(String productDescription);

    Product getProductByDescriptionEnglish(String productDescription);

    List<ProductDescription> getAllProductDescriptions(int page, int size);

    int getTotalNumberProducts();

    // Fooddiary method's:
    int getProductId(String lang,String description);

    int getUserIdByUsername(String username);

    int insertProductData(Product product);

    List<String> getAllDutchProductDescriptions();

    List<String> getAllEnglishProductDescriptions();

    String getMeasurementUnitByDescription(int productId);

    int insertProductIntoDiary(String lang, int userId, int productId, ProductEntry productEntry);

    List<ProductEntry> getDiaryEntriesByDate(String lang,int id, String date);

    int removeDiaryEntryById(int diaryEntryId);

    List<ProductOccurrence> getProductOccurrences(String lang);

}
