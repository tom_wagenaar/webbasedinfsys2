package nl.bioinf.fooddiary.restcontrol;

import nl.bioinf.fooddiary.FooddiaryApplication;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = FooddiaryApplication.class)
@AutoConfigureMockMvc
class ProductRESTControllerTest {

    @Autowired
    private MockMvc mockMvc;

    /**
     * This method calls other methods that execute tests for the getProductByDutchDescription method found in the
     * ProductRESTController.
     * @throws Exception
     */
    @Test
    void getProductByDutchDescriptionTest() throws Exception {
        getProductByDutchDescriptionTestSunny();
        getProductByDutchDescriptionTestRainy();
    }

    /**
     * Mock a database request using the /api/v1/products endpoint with path variables dutch and Advocaat. For the test
     * to succeed a OK status and a certain JSON response is expected, otherwise fail the test.
     * @throws Exception
     */
    private void getProductByDutchDescriptionTestSunny() throws Exception {
        String responseJson = "{\"code\":382,\"productGroup\":{\"groupCode\":2,\"groupCodeDescription\":\"Alcoholische dranken\"},\"productDescription\":{\"descriptionDutch\":\"Advocaat\",\"descriptionEnglish\":\"Advocaat liqueur\",\"synonymous\":\"_UNKNOWN_SYNONYMOUS_\"},\"productMeasurement\":{\"measurementUnit\":\"g\",\"measurementQuantity\":100,\"measurementComment\":\"Per 100g. Voor meer informatie zie Overzicht recepten op NEVO-website.\"},\"productInfoExtra\":{\"enrichedWith\":\"_UNKNOWN_INFO_\",\"tracesOf\":\"_UNKNOWN_INFO_\"},\"nutrientValues\":{\"nutrients\":[]}}";

        this.mockMvc
                .perform(get("/api/v1/products/dutch/Advocaat"))
                .andExpect(status().isOk())
                .andExpect(content().json(responseJson));
    }

    /**
     * Mock a database request using the /api/v1/products/ endpoint with path variables german and Advocaat. This test
     * is a rainy test, thus the test expects that a BadRequest status and an error message is returned.
     * @throws Exception
     */
    private void getProductByDutchDescriptionTestRainy() throws Exception {
        this.mockMvc
                .perform(get("/api/v1/products/german/Advocaat"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Called API end point contains langauge, german, that isn't supported. Please choose Dutch or English!"));
    }
}