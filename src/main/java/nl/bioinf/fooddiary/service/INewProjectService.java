package nl.bioinf.fooddiary.service;

import nl.bioinf.fooddiary.model.project.NewProject;

public interface INewProjectService {
    void addNewProject(NewProject newProject);
}
