package nl.bioinf.fooddiary.model.product;

import org.springframework.hateoas.RepresentationModel;

import java.util.List;

/**
 * @author Tom Wagenaar
 * @version 0.0.5
 * date: 01-09-2021
 *
 * Wrapper class that contains a list of ProductDescription objects, the total amount of objects an the amount of
 * descriptions shown. This class uses a typical builder pattern.
*/

public class ProductDescriptionWrapper extends RepresentationModel {
    // Instance variable declaration
    private final List<ProductDescription> descriptions;
    private final int totalAmountDescriptions;
    private final int shownAmountDescriptions;

    private ProductDescriptionWrapper(ProductDescriptionWrapperBuilder builder) {
        this.descriptions = builder.descriptions;
        this.totalAmountDescriptions = builder.totalAmountDescriptions;
        this.shownAmountDescriptions = builder.shownAmountDescriptions;
    }

    /**
     * Static method that serves an instance of the inner class ProductDescriptionWrapperBuilder, taking the required
     * arguments, see @param.
     * @param descriptions (list<ProductDescriptions>)
     * @param totalAmountDescriptions (int)
     * @param shownAmountDescriptions (int)
     * @return ProductDescriptionWrapperBuilder object
     */
    public static ProductDescriptionWrapperBuilder builder(List<ProductDescription> descriptions, int totalAmountDescriptions, int shownAmountDescriptions) {
        return new ProductDescriptionWrapperBuilder(descriptions, totalAmountDescriptions, shownAmountDescriptions);
    }

    // Getters
    public List<ProductDescription> getDescriptions() { return descriptions; }

    public int getTotalAmountDescriptions() { return totalAmountDescriptions; }

    public int getShownAmountDescriptions() { return shownAmountDescriptions; }

    @Override
    public String toString() {
        return "ProductDescriptionWrapper{" +
                "descriptions=" + descriptions +
                ", totalAmountDescriptions=" + totalAmountDescriptions +
                ", shownAmountDescriptions=" + shownAmountDescriptions +
                '}';
    }

    /**
     * Inner class that is used as a builder for the ProductDescriptionWrapper class.
     */
    public static class ProductDescriptionWrapperBuilder {
        // Required parameters
        private final List<ProductDescription> descriptions;
        private final int totalAmountDescriptions;
        private final int shownAmountDescriptions;

        private ProductDescriptionWrapperBuilder(List<ProductDescription> descriptions, int totalAmountDescriptions, int shownAmountDescriptions) {
            this.descriptions = descriptions;
            this.totalAmountDescriptions = totalAmountDescriptions;
            this.shownAmountDescriptions = shownAmountDescriptions;
        }

        /**
         * Serves the ProductDescription class
         * @return ProductDescriptionWrapper object with the parameters corresponding to the instance variable.
         */
        public ProductDescriptionWrapper build() {return new ProductDescriptionWrapper(this); }
    }
}
