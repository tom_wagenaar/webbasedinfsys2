package nl.bioinf.fooddiary.restcontrol;

import nl.bioinf.fooddiary.FooddiaryApplication;
import nl.bioinf.fooddiary.dao.ProductRepository;
import nl.bioinf.fooddiary.model.product.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Tom Wagenaar
 * Date: 16-05-2020
 *
 * This class is a REST API (REpresentional State Transfer), request are given by using different HTTP request on
 * different URLs. This class responses to requests at the /api/v1/.. URL. The methods in this class listen to different HTTP
 * requests, the responses of these method's are in JSON format.
 */
@RestController
@RequestMapping("/api/v1")
public class ProductRESTController {

    private static final Logger logger = LoggerFactory.getLogger(FooddiaryApplication.class);

    @Autowired
    private ProductRepository productRepository;

    /**
     * REST API method that where communication depends on the /api/v1/products URL (endpoint) and
     * /{descriptionLang}/{description} path variables. Where {descriptionLang} is either dutch or english and
     * {description} is the description that correspond to a Product description in the database that needs to be
     * retrieved. Return appropriate HttpStatus for errors.
     *
     * example endpoint with path variables: /api/v1/products/English/Taro raw
     *
     * expected output: {"code":382,"productGroup":{"groupCode":2,"groupCodeDescription":"Alcoholische dranken"},
     * "productDescription":{"descriptionDutch":"Advocaat","descriptionEnglish":"Advocaat liqueur","synonymous":"_UNKNOWN_SYNONYMOUS_"},
     * "productMeasurement":{"measurementUnit":"g","measurementQuantity":100,"measurementComment":"Per 100g. Voor meer informatie zie Overzicht recepten op NEVO-website."},
     * "productInfoExtra":{"enrichedWith":"_UNKNOWN_INFO_","tracesOf":"_UNKNOWN_INFO_"},"nutrientValues":{"nutrients":[]}}
     *
     * @param descriptionLang (String) Containing the language the description is in.
     * @param description (String) Product description that needs to be find in the database.
     * @return ResponseEntity<Product> The response back to the resource containing a Product object and a HttpStatus object.
     */
    @RequestMapping(value = "/products/{descriptionLang}/{description}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity getProductByDutchDescription(@PathVariable String descriptionLang, @PathVariable String description) {

        logger.info("/api/v1/products endpoint called!");

        Product product;

        try {

            // Find the Product with description in dutch.
            if (descriptionLang.equals("dutch") | descriptionLang.equals("Dutch")) {
                product = productRepository.getProductByDescriptionDutch(description);
                logger.info("/api/v1/products/ endpoint with path variable: " + descriptionLang + ", retrieving Product " +
                        "corresponding to description: " + description);

                // Find the Product with description in english
            } else if (descriptionLang.equals("english") | descriptionLang.equals("English")) {
                product = productRepository.getProductByDescriptionEnglish(description);
                logger.info("/api/v1/products/ endpoint with path variable: " + descriptionLang + ", retrieving Product " +
                        "corresponding to description: " + description);

                // Return bad request, whenever the path variable isn't dutch or english.
            } else {
                logger.info("/api/v1/products endpoint called, " + descriptionLang + " ins't dutch or english! returning" +
                        " Bad Request!");
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Called API end point contains langauge, " + descriptionLang + ", that isn't supported. Please choose Dutch or English!");
            }


            // If the description isn't in the database return a bad request.
        } catch (EmptyResultDataAccessException exception) {
            logger.info("/api/v1/products/ endpoint with path variable: " + descriptionLang + ", Product: " +
                    description + " isn't in the database! BAD_REQUEST!");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Called API end point contains langauge, " + descriptionLang + ", that isn't supported. Please choose Dutch or English!");
        }

        return ResponseEntity.status(HttpStatus.OK).body(product);
    }
}